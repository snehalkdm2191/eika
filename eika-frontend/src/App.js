import "./css/App.css";
import React from "react";
import WelcomeScreen from "./components/ShoppingList/WelcomeScreen";
import NormalScreen from "./components/ShoppingList/NormalScreen";

function App() {
  const localStorageLength = window.localStorage.length;
  let itemList;
  let itemListCount = 0;
  if (localStorageLength !== 0) {
    itemList = JSON.parse(localStorage.getItem("Items"));
    itemListCount = itemList.length;
  }
  return (
    <>
      <img className="img-logo" src="../../img/logo.png" alt="" />
      <div className="container mt-5">
        {itemListCount === 0 ? (
          <WelcomeScreen></WelcomeScreen>
        ) : (
          <NormalScreen></NormalScreen>
        )}
      </div>
    </>
  );
}

export default App;
