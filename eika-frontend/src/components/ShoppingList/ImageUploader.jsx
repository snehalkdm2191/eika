import React, { useState } from "react";
import { storage } from "../../firebase";

//uploader to add all type of files.
function ImageUploader({ setFileUrl }) {
  const [progress, setProgress] = useState(0);
  const [file, setFile] = useState(null);

  const localStorageLength = window.localStorage.length;
  let itemList = 0;
  let fileId = 0;
  if (localStorageLength !== 0) {
    itemList = JSON.parse(window.localStorage.getItem("Items"));
    if(itemList.length !== 0)
    {
      fileId = itemList.length + 1;
    }
  }

  const handleChange = (e) => {
    if (e.target.files[0]) {
      var fileName = e.target.files[0].name;
      document.getElementById("upload-label").innerHTML = fileName;
      setFile(e.target.files[0]);
    }
  };

  const handleUpload = () => {
    if (file === null) {
      alert("Please select a file to upload");
    } else {
      const uploadTask = storage.ref(`files/${fileId + file.name}`).put(file);
      uploadTask.on(
        "state_changed",
        (snapshot) => {
          const progress = Math.round(
            (snapshot.bytesTransferred / snapshot.totalBytes) * 100
          );
          setProgress(progress);
        },
        (error) => {
          console.log(error);
        },
        () => {
          storage
            .ref("files")
            .child(fileId + file.name)
            .getDownloadURL()
            .then((url) => {
              setFileUrl(url);
            });
        }
      );
    }
  };

  console.log("file: ", file);
  return (
    <div>
      <div class="input-group mb-3 px-2 py-2 rounded-pill bg-white shadow-sm">
        <input
          id="upload"
          type="file"
          className="form-control border-0"
          onChange={handleChange}
        />
        <label
          id="upload-label"
          for="upload"
          class="font-weight-light text-muted"
        >
          Choose file
        </label>
        <div class="input-group-append">
          <label for="upload" class="btn btn-light m-0 rounded-pill px-4">
            <i class="fa fa-cloud-upload mr-2 text-muted"></i>
            <button class="btnUpload font-weight-bold text-muted" onClick={handleUpload}>Upload</button>
          </label>
        </div>
      </div>
      <progress className="progress" value={progress} max="100" />
    </div>
  );
}

export default ImageUploader;
