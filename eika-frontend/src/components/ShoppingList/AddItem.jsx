import React, { useState } from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import { useItems } from "./StorePage";
import ImageUploader from "./ImageUploader";

const ItemInput = (props) => {
  //Add data to local storage
  const { addItem } = useItems();
  const [fileUrl, setFileUrl] = useState("");
  const [newItem, setNewItem] = useState("");
  const [newPrice, setNewPrices] = useState("");
  const onClick = () => {
    console.log("img" + fileUrl);
    addItem(newItem, newPrice, fileUrl);
    setNewItem("");
    setNewPrices("");
    setFileUrl("");
    handleNextClose();
    window.location.reload();
  };

  //Popup window handler code
  const [show, setShow] = useState(false);
  const [showNext, setShowNext] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const handleNextShow = () => {
    setShow(false);
    setShowNext(true);
  };
  const handleNextClose = () => {
    setShowNext(false);
  };

  //validate price input
  const validatePrice = /^[+-]?\d*(?:[.,]\d*)?$/;

  return (
    <>
      <div>
        <button
          type="button"
          className="btn-add"
          data-toggle="modal"
          onClick={handleShow}
        >
          Add Item
        </button>
      </div>
      <Modal className="Model-top-position" show={show} onHide={handleClose}>
        <Modal.Body>
          <input
            className="frm-input"
            placeholder="Enter item name.."
            variant="outlined"
            size="small"
            value={newItem}
            onChange={(e) => setNewItem(e.target.value)}
          />
        </Modal.Body>
        <Modal.Footer>
          <Button className="btn-frm-close" variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button
            className="btn-frm-add"
            disabled={newItem.length === 0}
            onClick={handleNextShow}
          >
            Next
          </Button>
        </Modal.Footer>
      </Modal>

      <Modal
        className="Model-top-position"
        show={showNext}
        onHide={handleNextClose}
      >
        <Modal.Body>
          <input
            className="frm-input"
            placeholder="Enter item price.."
            variant="outlined"
            size="small"
            value={newPrice}
            onChange={(e) =>
              validatePrice.test(e.target.value)
                ? setNewPrices(e.target.value)
                : alert("Please enter valide data..")
            }
          />
          <ImageUploader setFileUrl={setFileUrl} />
        </Modal.Body>
        <Modal.Footer>
          <Button className="btn-frm-close" variant="secondary" onClick={handleNextClose}>
            Close
          </Button>
          <Button
            className="btn-frm-add"
            disabled={newPrice.length === 0}
            onClick={onClick}
          >
            Add
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default ItemInput;
