import React from "react";
import AddItem from "./AddItem";
import ItemList from "./ItemList";
import { withProvider } from "./StorePage";

const Items = () => {
  return (
    <div className="cart-section">
      <AddItem />
      <br/>
      <ItemList /> 
    </div>
  );
};

export default withProvider(Items);
