import React, { useContext, createContext } from "react";
import useLocalStorage from "../useLocalStorage";

const Context = createContext({
  Items: [],
});

const Provider = (props) => {
  const { children } = props;
  const [Items, setItems] = useLocalStorage("Items", []);
  const addItem = (name, price, img) => {
    const nextId =
      Items.length > 0 ? Math.max(...Items.map((t) => t.id)) + 1 : 0;
    const newItem = {
      id: nextId,
      name,
      price,
      img,
      completed: false,
    };
    setItems([...Items, newItem]);
  };
  const removeItem = (id) => {
    const newItems = Items.filter((t) => t.id !== id);
    setItems(newItems);
    window.location.reload()
  };
  const toggleItem = (id) => {
    const foundItem = Items.find((t) => t.id === id);
    if (foundItem) {
      foundItem.completed = !foundItem.completed;
    }
    const newItems = Items.map((t) => {
      if (t.id === id) {
        return foundItem;
      }
      return t;
    });
    setItems(newItems);
  };
  return (
    <Context.Provider value={{ Items, addItem, removeItem, toggleItem }}>
      {children}
    </Context.Provider>
  );
};

export const useItems = () => useContext(Context);

export const withProvider = (Component) => {
  return (props) => {
    return (
      <Provider>
        <Component {...props} />
      </Provider>
    );
  };
};