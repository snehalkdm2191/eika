import React from "react";
import AddItem from "./AddItem";
import { withProvider } from "./StorePage";

const FirstItem = () => {
  return (
    <div>
      <div className="home-bg-container">
        <img
          className="img-home-bg"
          src="../../img/bg1.gif"
          width="600"
          height="540"
          alt=""
        />
        <div className="home-content">
          <p>
            Welcome to EIKA Sweden. Here you will find everything in interior
            design, furniture, white goods and inspiration. Also find your local
            IKEA department store or shop online.
            <br /> You can start shopping by adding item in your cart. Add item
            name, price and picture. You can see all item details in your cart .
            If order completed you can select item and move to complete order
            section.
          </p>
          <br />
           <AddItem></AddItem>
        </div>
      </div>
    </div>
  );
}

export default withProvider(FirstItem);
