import React, { useState, useMemo } from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import { useItems } from "./StorePage";

const ItemList = () => {
  const [filter, setFilter] = useState("all");
  const [pendingItem] = useState("not_completed");
  const { Items, toggleItem, removeItem } = useItems();

  //Fileter product section
  const cartItems = useMemo(() => {
    if (pendingItem === "not_completed") {
      return Items.filter((t) => !t.completed);
    }
  }, [Items, pendingItem]);

  const filteredItems = useMemo(() => {
    if (filter === "completed") {
      return Items.filter((t) => t.completed);
    }
  }, [Items, filter]);

  //Toggle Complete data section
  const [isVisible, setIsVisible] = React.useState(false);
  const toggleVisibility = () => setIsVisible(!isVisible);

  //Popup window show or close section
  const [show, setShow] = useState(false);
  const [imgSrc, setImgSrc] = useState("");
  const handleClose = () => {
    setShow(false);
    window.location.reload();
  };
  const handleShow = (val) => {
    setImgSrc(val);
    setShow(true);
  };

  //Sort data section
  const [currentSort, setCurrentSort] = useState("default");

  const sortTypes = {
    up: {
      class: "sort-up",
      fnPrice: (a, b) => a.price - b.price,
      fnName: (a, b) => a.name.localeCompare(b.name),
    },
    down: {
      class: "sort-down",
      fnPrice: (a, b) => b.price - a.price,
      fnName: (a, b) => b.name.localeCompare(a.name),
    },
    default: {
      class: "sort",
      fnName: (a, b) => a,
      fnPrice: (a, b) => a,
    },
  };
  const onSort = () => {
    let nextSort;

    if (currentSort === "down") nextSort = "up";
    else if (currentSort === "up") nextSort = "default";
    else if (currentSort === "default") nextSort = "down";

    setCurrentSort(nextSort);
  };

  const onNameSort = () => {
    onSort();
    return cartItems.sort(sortTypes[currentSort].fnName);
  };

  const onPriceSort = () => {
    onSort();
    return cartItems.sort(sortTypes[currentSort].fnPrice);
  };

  return (
    <>
      <table className="table">
        <thead>
          <tr>
            <th>
              {" "}
              name
              <button className="btnSort" onClick={onNameSort}>
                <i className={`fas fa-${sortTypes[currentSort].class}`} />
              </button>
            </th>
            <th>
              Price
              <button className="btnSort" onClick={onPriceSort}>
                <i className={`fas fa-${sortTypes[currentSort].class}`} />
              </button>
            </th>
            <th>Image</th>
            <th></th>
            <th></th>
          </tr>
        </thead>
        {cartItems.map((Item) => {
          return (
            <tbody key={Item.id}>
              <tr className="text-right">
                <td>{Item.name}</td>
                <td>{Item.price}</td>
                <td value={Item.img} onClick={() => handleShow(Item.img)}>
                  {Item.img === "" ? (
                    <img
                      src="../../img/No_Image_Available.png"
                      width="70"
                      height="50"
                      alt=""
                    />
                  ) : (
                    <img src={Item.img} width="70" height="50" alt="" />
                  )}
                </td>
                <td>
                  <input
                    type="checkbox"
                    checked={Item.completed}
                    onClick={() => toggleItem(Item.id)}
                  />
                </td>
                <td>
                  <button
                    className="btn-remove"
                    onClick={() => removeItem(Item.id)}
                  >
                    <i class="fas fa-trash-alt"></i>
                  </button>
                </td>
              </tr>
            </tbody>
          );
        })}
      </table>

      <Modal
        className="Model-top-position"
        show={show}
        imgSrc={imgSrc}
        onHide={handleClose}
      >
        <Modal.Body>
          <img src={imgSrc} width="400" height="350" alt="" />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>

      <button
        className="btn-complete"
        onClick={() => {
          setFilter("completed");
          toggleVisibility();
        }}
      >
        <i class="far fa-eye-slash"></i> View Completed Items
      </button>

      <table className="table">
        {isVisible
          ? filteredItems.map((Item) => {
              return (
                <tbody key={Item.id}>
                  <tr>
                    <td>{Item.name}</td>
                    <td>{Item.price}</td>
                    <td>
                      <img src={Item.img} width="80" height="60" alt="" />
                    </td>
                    <td></td>
                  </tr>
                </tbody>
              );
            })
          : null}
      </table>
    </>
  );
};

export default ItemList;
