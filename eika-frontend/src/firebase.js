import firebase from "firebase/app";
import "firebase/storage";

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyBfrw1A7I4x0P7jQz4p4oRSxj1XP2C3zeY",
  authDomain: "eika-shopping.firebaseapp.com",
  projectId: "eika-shopping",
  storageBucket: "eika-shopping.appspot.com",
  messagingSenderId: "626728489075",
  appId: "1:626728489075:web:cbf2d1451d097c76875a06",
  measurementId: "G-6S61J0Q3JE",
};

firebase.initializeApp(firebaseConfig);

const storage = firebase.storage();

export { storage, firebase as default };
