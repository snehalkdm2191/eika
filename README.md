# Eika
Welcome to EIKA Sweden. Here you will find everything in interior
design, furniture, white goods and inspiration. Also find your local
IKEA department store or shop online.\
You can start shopping by adding item in your cart. Add item
name, price and picture. You can see all item details in your cart .
If order completed you can select item and move to complete order
section.

### `Project Requiremets document`

The document given by the company as the coding test. Important notes like functional requirements, non-functional requirements, user data, extra to add on and must do things are highlighted in the document.

Document link : \
Open [Project requiremets pdf](https://gitlab.com/snehalkdm2191/eika/-/blob/main/Documents/Shopping%20list.pdf) to view it in the browser.

### `Project spreadsheet`

The spreadsheet to see the details of requirements which is gathered by analyzing the provided document data.

Project spreadsheet link : \
Open [Eika project spreadsheet](https://gitlab.com/snehalkdm2191/eika/-/tree/main/Documents) to view it in the browser.

### `Design mockup`

Figma Templte design to visualize the app. To see how the app is going to look.

Figma design link : \
Open [Eika figma design](https://www.figma.com/file/ZXvVkQ2qs74XhHKzkehZN1/EIKA?node-id=0%3A1) to view it in the browser.

### `Component tree diagram`

To visualize the components we are going to use in the app. 

Component Tree diagram link : \
Open [Eika component tree digram](https://whimsical.com/eika-shopping-JrDfN8v2VojNceY7LT475x) to view it in the browser.


### `Eika hosting Link`

Hosting link : \
Open [https://eika-shopping.web.app/](https://eika-shopping.web.app/) to view it in the browser.



